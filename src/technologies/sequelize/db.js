const Sequelize = require('sequelize')

const connection = new Sequelize('postgres', 'postgres', '', {
  host: '127.0.0.1',
  dialect: 'postgres',
  operatorsAliases: false,
  logging: console.log,

  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  }
});

// test_connection();
module.exports.db = connection

function test_connection() {
  connection
    .authenticate()
    .then(() => {
      console.log('Connection has been established successfully.');
    })
    .catch(err => {
      console.error('Unable to connect to the database:', err);
    });
}
