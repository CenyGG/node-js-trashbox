import {db} from "./db";
import {DataTypes} from "sequelize";

export const Currency = db.define("currency", {
    name: {
        type: DataTypes.STRING,
        allowNull: false,
        noUpdate: true
    },
    fullname: {
        type: DataTypes.STRING,
        allowNull: false,
        noUpdate: true
    }
});


