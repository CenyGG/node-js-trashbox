import {DataTypes} from "sequelize";
import {db} from "./db";

export const Balance = db.define("balance", {
    userId: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {model: "users", key: "id"}
    },
    currencyId: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {model: "currencies", key: "id"}
    },
    amount: {
        type: DataTypes.DECIMAL,
        defaultValue: 0,
        validate: {min: 0}
    },
    bonus: {
        type: DataTypes.DECIMAL,
        defaultValue: 0,
        validate: {min: 0}
    },
    held: {
        type: DataTypes.DECIMAL,
        defaultValue: 0,
        validate: {min: 0}
    },
    trader: {
        type: DataTypes.ENUM("deposit", "withdrawal"),
    },
    trader_a: {
        type: DataTypes.ENUM("deposit1", "withdrawal1"),
    }
}, {
    indexes: [
        {unique: true, fields: ["userId", "currencyId"]}
    ]
});
