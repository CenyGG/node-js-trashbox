import {DataTypes} from "sequelize";
import {Trader} from "./trader";
import {Balance} from "./balance";
import {db} from "./db";

export const Portfolio = db.define("portfolio", {
    traderId: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {model: "traders", key: "id"}
    },
    currencyId: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {model: "currencies", key: "id"}
    }
}, {
    hooks: {
        beforeCreate: async (instance, option) => {
            const trader = await Trader.findById(instance.traderId)
            return db.transaction(async () => {
                await Balance.create({userId: trader.userId, currencyId: this.currencyId, trader: "deposit"})
                await Balance.create({userId: trader.userId, currencyId: this.currencyId, trader: "withdrawal"})
            })
        }
    },
    indexes: [
        {fields: ["traderId"]},
        {fields: ["currencyId"]}
    ],
});