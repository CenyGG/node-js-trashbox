import {DataTypes} from "sequelize";
import {Balance} from "./balance";
import {db} from "./db";

export const Trader = db.define("trader", {
    userId: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {model: "users", key: "id"}
    },
    name: {
        type: DataTypes.STRING,
        allowNull: false,
        noUpdate: true
    },
    estimateBalance: {
        type: DataTypes.DECIMAL,
        defaultValue: 0
    }
}, {
    indexes: [
        {unique: true, fields: ["userId"]}
    ],
    hooks: {
        beforeCreate(instance, options){
            Balance.create({userId, currencyId})
        },
        beforeValidate(instance, options) {
            if (!options.validate || instance.isNewRecord) return
            Object
                .keys(instance._changed)
                .forEach(function (fieldName) {
                    if (instance._changed[fieldName]) {
                        const fieldDefinition = instance.rawAttributes[fieldName];
                        if (!fieldDefinition['noUpdate']) {
                            return;
                        }
                        if (instance._previousDataValues[fieldName] !== undefined && instance._previousDataValues[fieldName] !== null) {
                            return db.Promise.reject(new Error(`Can't update ${fieldName} because of update restrictions.`))
                        }
                    }
                });
        }
    }
});
