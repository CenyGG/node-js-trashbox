import {DataTypes} from "sequelize";
import {db} from "./db";

export const VerificationRequest = db.define("deposit", {
    userId: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {model: "users", key: "id"}
    },
    name: {
        type: DataTypes.STRING,
        notEmpty: true,
        allowNull: false
    },
    lastName: {
        type: DataTypes.STRING,
        notEmpty: true,
        allowNull: false
    },
    birthDate: {
        type: DataTypes.DATE,
        allowNull: false
    },
    country: {
        type: DataTypes.STRING,
        notEmpty: true,
        allowNull: false
    },
    doc1Expires: {
        type: DataTypes.DATE,
        allowNull: false
    },
    address: {
        type: DataTypes.STRING,
        notEmpty: true,
        allowNull: false
    },
    city: {
        type: DataTypes.STRING,
        notEmpty: true,
        allowNull: false
    },
    postCode: {
        type: DataTypes.STRING,
        notEmpty: true,
        allowNull: false
    },
    addressCountry: {
        type: DataTypes.STRING,
        notEmpty: true,
        allowNull: false
    },
    doc2Expires: {
        type: DataTypes.DATE,
        allowNull: false
    },
    doc1Name: {
        type: DataTypes.STRING,
        notEmpty: true,
        defaultValue: false
    },
    doc2Name: {
        type: DataTypes.STRING,
        notEmpty: true,
        defaultValue: false
    }
}, {
    indexes: [
        {fields: ["userId", "country", "doc1Name", "doc2Name"]}
    ]
});
