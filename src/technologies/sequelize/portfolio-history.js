import {DataTypes} from "sequelize";
import {db} from "./db";

const PortfolioHistory = db.define("portfoliohistory", {
    portfolioId: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {model: "portfolios", key: "id"}
    },
    _date: {
        type: DataTypes.DATE,
        allowNull: false
    },
    positionsCount: {
        type: DataTypes.INTEGER,
        defaultValue: 0
    },
    profit: {
        type: DataTypes.DECIMAL,
    }
}, {
    indexes: [
        {unique: true, fields: ["portfolioId"]}
    ],
});

module.exports = PortfolioHistory