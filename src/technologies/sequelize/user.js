import {db} from "./db";
import {DataTypes} from "sequelize";

export const User = db.define("user", {
    name: {
        type: DataTypes.STRING,
        allowNull: false,
        noUpdate: true
    },
    fullname: {
        type: DataTypes.STRING,
        allowNull: false,
        noUpdate: true
    }
});


