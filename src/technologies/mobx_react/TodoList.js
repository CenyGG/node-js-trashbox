import React, {Component} from 'react'
import {inject} from 'mobx-react'

@inject('store')
class TodoList extends Component{
    render(){
        return (
            <div className="todoList">
                {this.props.store.todos.map((elem)=>{<TodoItem name={elem}/>})}
                <Button onClick={this.handleClick}></Button>
                Array
            </div>
        );
    }
    handleClick(){
        const {store} = this.props;
        store.map.set(1,"123213123")
    }
}

class TodoItem extends Component{
    render(){
        return (
            <div className='todoItem'>
                <div>{this.props.name}</div>
            </div>
        )
    }
}

export default TodoList
