import {observable} from 'mobx'

class Todos{
    @observable todos = []
    map = observable.map({}, {name: 'todos'})
}
let store = new Todos();
store.todos.push("1");
store.todos.push("22");
store.todos.push("333");

export default new Todos();
