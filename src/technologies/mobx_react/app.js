import React from 'react';
import ReactDOM from 'react-dom';
import TodoList from 'src/technologies/mobx_react/TodoList'
import store from 'src/technologies/mobx_react/store'
import {Provider} from 'mobx-react'

ReactDOM.render(
    <Provider {store}>
        <TodoList/>
    </Provider>
    , document.body);