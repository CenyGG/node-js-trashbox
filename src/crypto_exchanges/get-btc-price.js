function getBtcPrice(prices, currency) {
    return (currency === 'BTC') ? 1
        : prices[`${currency}/BTC`] ? prices[`${currency}/BTC`]
            : prices[`BTC/${currency}`] ? 1 / prices[`BTC/${currency}`]
                : prices[`${currency}/USDT`] ? prices[`${currency}/USDT`] / prices[`BTC/USDT`]
                    : prices[`${currency}/USD`] ? prices[`${currency}/USD`] / prices[`BTC/USDT`]
                        : 0
}