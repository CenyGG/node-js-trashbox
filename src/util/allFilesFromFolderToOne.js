const fs = require('fs')
const path = require('path')

/**
 * Merge all files from ${folder} to one file named ${resFile}
 * */
function mergeFiles(folder, resFile) {
    const filesArray = fs
        .readdirSync(folder)
        .map(e => path.resolve(e))
        .filter(e => fs.lstatSync(e).isFile())
    _mergeFiles(filesArray, resFile)
}

function _mergeFiles(filesArray, resFile) {
    const file = filesArray.pop()
    if (!file) return

    const writeStream = fs.createWriteStream(`./${resFile}`, {flags: 'a'})

    const readStream = fs.createReadStream(`${file}`)
    const stream = readStream.pipe(writeStream)

    stream.on('finish', () => {
        readStream.destroy()
        writeStream.destroy()
        _mergeFiles(filesArray, resFile)
    });
}

function test() {
    mergeFiles('./', 'shit')
}
