const Agent = require('https').Agent
const HttpsProxyAgent = require('https-proxy-agent')
const log = require('../../lib/log')
const getProxies = require('./getProxies')

const PROXY_REFRESH_TIME = 5 * 60 * 1000

// todo why this shit not working
class HttpsProxyListAgent extends Agent {
    constructor({ proxyList, client }) {
        super({ keepAlive: false, maxSockets: 10, maxFreeSockets: 40 })
        this.ready = false
        this.refreshProxyList(proxyList, client).then(() => log.info('Proxy List created'))
    }

    get sockets() {
        const next = this.next
        if (next === this) return this._sockets
        return next.sockets
    }

    set sockets(val) {
        this._sockets = val
    }

    get requests() {
        const next = this.next
        if (next === this) return this._requests
        return next.requests
    }

    set requests(val) {
        this._requests = val
    }

    proxyOne(methods) {
        for (let method of methods) {
            this[method] = (...args) => {
                const agent = this.next
                return agent[method](...args)
            }
        }
    }

    proxyAll(methods) {
        methods.forEach(method => {
            this[method] = (...args) => {
                this.proxyAgents.forEach(agent => {
                    agent[method](...args)
                })
            }
        })
    }

    get next() {
        if (this.proxyAgents.length === 0) return this
        const index = this.proxyIndex === this.proxyAgents.length - 1 ? 0 : ++this.proxyIndex
        return this.proxyAgents[index]
    }

    async refreshProxyList(proxyList, client) {
        if (!proxyList) {
            proxyList = await getProxies()
            proxyList = proxyList.map(proxy => `${proxy.ipAddress}:${proxy.port}`)
            setTimeout(this.refreshProxyList, PROXY_REFRESH_TIME)
        }
        this.proxyIndex = 0
        this.proxyAgents = proxyList.map(proxy => {
            return new HttpsProxyAgent({
                host: proxy,
                protocol: 'https:',
                keepAlive: false,
                maxSockets: 10,
                maxFreeSockets: 40
            })
        })
        this.proxyAll(['removeListener', 'removeAllListeners', 'destroy'])
        this.proxyOne(['addRequest'])
        this.ready = true
        client.enableRateLimit = false
        log.info(`Found ${proxyList.length} free proxies.`)
    }
}

module.exports = HttpsProxyListAgent
