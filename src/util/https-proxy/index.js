const url = require('url')
const https = require('https')
const HttpsProxyAgent = require('https-proxy-agent')
const getProxies = require('./getProxies')

const proxyGo = 'http://http-proxy-go.herokuapp.com/'
const proxyNode = 'http://http-proxy-node.herokuapp.com/'

const whoAmI = 'https://echo-ip-server.herokuapp.com/'
const options = url.parse(whoAmI)

overMyProxy()

function overMyProxy() {
    const agentGo = new HttpsProxyAgent(proxyGo)
    const agentNode = new HttpsProxyAgent(proxyNode)

    const optionsGo = Object.assign({}, options, {agent: agentGo})
    const optionsNode = Object.assign({}, options, {agent: agentNode})

    makeRequest(optionsGo)
    makeRequest(optionsNode)
}

function overProxy() {
    Promise.resolve().then(async () => {
        const proxies = await getProxies()
        console.log({
            proxies,
            len: proxies.length
        })

        for (let proxy of proxies) {
            const agent = new HttpsProxyAgent({
                host: proxy.ipAddress,
                port: proxy.port,
            })
            const optionsWithAgent = Object.assign({}, options, {agent})

            makeRequest(optionsWithAgent)
        }
    })
}


function makeRequest(options) {
    const req = https.request(options, (res) => {
        console.log('statusCode:', res.statusCode);
        console.log('headers:', res.headers);

        res.on('data', (d) => {
            process.stdout.write("chunk_\r\n");
            process.stdout.write(d);
        });
    });

    req.on('error', (e) => {
        console.error(e);
    });
    req.end();
}
