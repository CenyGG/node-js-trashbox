const ProxyLists = require('proxy-lists');

module.exports = function() {
    return new Promise((h,r)=>{
        const buffer=[]
        ProxyLists.getProxies({
            countries: ['eu', 'ru', 'de'],
            protocols: ['https'],
            ipTypes: ['ipv4'],
            sourcesBlackList: ['bitproxies', 'spys-one', 'workingproxies-org', 'hugeproxies', 'proxies24']
        })
            .on('data', function(proxies) {
                buffer.push(...proxies)
            })
            .on('error', function(error) {
                console.log('Ooops some error, nvm.')
                // console.warn(error)
            })
            .once('end', function() {
                h(buffer)
            });
    })
}