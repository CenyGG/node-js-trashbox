'use strict'


/**
 * This code about showing performance difference 
 * between closure and method.bind() operations
 * 
 * Also decorators.
 * 
 * Spoiler : practically no difference.
 * mb bind() wins 5% performance
 * 
 * Here is version for babel transform-decorators-legacy to work with decorators.
 * But also exists shit4.js with pure node run
 * 
 */

function logTime(target, name, descriptor) {
    const originalFunction = descriptor.value;
    descriptor.value = (...args)=>{
        let start = new Date();
        console.log(start);
        originalFunction.apply(this, args);
        console.log(`Time : ${+new Date() - +start}`);
        console.log();
    };

    return;
}


class A {
    @logTime
    longClosure() {
        console.log("Closure : with args:"+[].slice.call(arguments))
        for (let a = 0; a < 1000000; a++)
            (() => { pureWork() })();
    }

    @logTime
    longBind() {
        console.log("Bind :"+[].slice.call(arguments))
        for (let a = 0; a < 1000000; a++)
            pureWork.bind(A)();
    }
}

function pureWork() {
    for (let i = 0; i < 1000; i++) {
        let t = i * i;
    }
}



let a = new A();
a.longBind(1,2,3);
a.longClosure(1,2,3);
