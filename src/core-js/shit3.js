'use strict'

/**
 * 
 * It is about showing asyncronosly working of JS functions using acync await
 * 
 */

function sleep(milis) {
    return new Promise((res, rej) => setTimeout(res, milis));
}


async function a() {
    b();
}

async function b() {
    log("Before wait");
    await sleep(5000);
    log("After wait");
}

async function executeA() {
    log("Before A");
    await a();
    log("After A")
}

function log(mes) {
    console.log(new Date() + " " + !!mes ? mes : "");
}


executeA();
log("After execute all");