'use strict'


/**
 * This code about showing performance difference 
 * between closure and method.bind() operations
 * 
 * 
 * Spoiler : practivali no difference.
 * mb bind() wins 5% performance
 * 
 */

class A {
    longClosure() {
        let start = new Date();
        console.log(start);

        console.log("Closure : with args:" + [].slice.call(arguments))
        for (let a = 0; a < 1000000; a++)
            (() => { pureWork() })();

        console.log(`Time : ${+new Date() - +start}`);
        console.log();
    }

    longBind() {
        let start = new Date();
        console.log(start);

        console.log("Bind :" + [].slice.call(arguments))
        for (let a = 0; a < 1000000; a++)
            pureWork.bind(A)();

        console.log(`Time : ${+new Date() - +start}`);
        console.log();
    }
}

function pureWork() {
    for (let i = 0; i < 1000; i++) {
        let t = i * i;
    }
}



let a = new A();
a.longBind(1, 2, 3);
a.longClosure(1, 2, 3);
