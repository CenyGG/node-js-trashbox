const net = require('net')
const tls = require('tls')
const http = require('http')
const https = require('https')
const url = require('url')

const _url = url.parse("https://www.google.com")
const _port = 443

console.log(JSON.stringify(_url))

// for (let _fun of [testNet, testHttp, testHttpConnect, testHttps, testHttpsConnect]) {
//     try {
//         _fun()
//     } catch (e) {
//         console.log(e)
//     }
// }

testTls()
testNet()

function testNet() {
    const socket = net.connect(_port, _url.host, () => {
        log("net")
    }).on('data', (data) => {
        log('net', data)
    })

    socket.write("GET /hello.htm HTTP/1.1\r\n\r\n")
}

function testTls() {
    const socket = tls.connect(_port, _url.host, () => {
        log("tls")
    }).on('data', (data) => {
        log('tls', data)
    }).on('connect', ()=>{
        log("tls connecting")
    })

    socket.write("GET /hello.htm HTTP/1.1\r\n\r\n")
}

function testHttp() {
    const options = {
        hostname: _url.host,
        port: _port,
        method: 'GET',
        path: '/'
    }
    http.request(options, (res) => {
        log("http.get", res)
        res.on('data', (data) => {
            log("http.get.data", data)
        })
    })
}

function testHttp2() {
    http.get(_url, (res) => {
        log("http.get", res)
        res.on('data', (data) => {
            log("http.get.data", data)
        })
    })
}

function testHttpConnect() {
    const options = {
        hostname: _url.host,
        port: _port,
        method: 'CONNECT',
        path: '/'
    }
    http.request(options, (res) => {
        log("http.connect")
        res.on('data', (data) => {
            log("http.connect.data", data)
        })
    })
}

function testHttps() {
    const options = {
        hostname: _url.host,
        port: _port,
        method: 'GET',
        path: '/'
    }
    http.request(options, (res) => {
        log("https.get")
        res.on('data', (data) => {
            log("https.get", data)
        });
    })
}

function testHttpsConnect() {
    const options = {
        hostname: _url.host,
        port: _port,
        method: 'CONNECT',
        path: '/'
    }
    http.request(options, (res) => {
        log('https.connect')

        res.on('data', (data) => {
            log('https.connect', data)
        });
    })
}


function log(msg) {
    console.log(msg)
}