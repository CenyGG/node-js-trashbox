class A {
    a() {
        console.log("a")
    }
    toString() {
        return "This is A: "
    }
}
A.prototype.a_prot = {
    q: "q"
}
A.prototype.l_fun = () => {
    console.log(this + ": l_fun")
}

class B extends A {
    b() {
        console.log("b")
    }
    toString() {
        return "This is B: "
    }
}

B.prototype.obj = {
    qb: "qb",
    fun: function () {
        console.log(this + ": fun")
    },
    l_fun: () => {
        console.log(this + ": lambda")
    },
    callback_l_fun: () => () => {
        console.log(this + ": lambda")
    },
    toString: function () { return "Obj inside prototype: " }

}
B.prototype.fun = function () {
    console.log(this + ": B prot fun")
}
B.prototype.l_fun = () => {
    console.log(this + ": B prot l_fun")
}
B.prototype.callback_l_fun = () => () => {
    console.log(this + ": B prot double callback l_fun")
}

const b = new B();

console.log("B.prototype instanceof A : " + (B.prototype instanceof A) )

console.log("\nJust func:")
b.fun()
b.l_fun()
b.callback_l_fun()()

console.log("\nFunc inside object:")
b.obj.fun()
b.obj.l_fun()
b.obj.callback_l_fun()()

console.log("\nFunc inside object using call:")
b.obj.fun.call(b)
b.obj.l_fun.call(b)
b.obj.callback_l_fun().call(b)
