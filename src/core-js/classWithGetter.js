class A {
    constructor(){
        this.a = 'suka'
    }
    doStuff(){
        console.log(this.a)
    }
}

class B extends A {
    constructor(a){
        super()
        this._a = a
    }
    get a(){
        return this._a
    }

    set a(val){
        this._a = val
    }
}


let b = new B('blad')
b.doStuff()