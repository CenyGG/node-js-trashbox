const fs = require('fs')
const util = require('util')
const { argv } = require('yargs')

const pid = process.pid
const FETCH_HEAD = `${__dirname}/../.git/FETCH_HEAD`

const commit = !fs.existsSync(FETCH_HEAD) ? '******' : fs
    .readFileSync(FETCH_HEAD)
    .toString()
    .slice(0, 6)

const LEVELS = {
    INFO: 'info',
    WARN: 'warn',
    ERROR: 'error',
    DEBUG: 'debug'
}

const COLORS = {
    GREEN: '\x1b[32m',
    RED: '\x1b[31m',
    BLUE: '\x1b[34m',
    YELLOW: '\x1b[33m',
    DEFAULT: '\x1b[0m'
}

const colorize = {
    info: str => `${COLORS.GREEN}${str}${COLORS.DEFAULT}`,
    warn: str => `${COLORS.YELLOW}${str}${COLORS.DEFAULT}`,
    error: str => `${COLORS.RED}${str}${COLORS.DEFAULT}`,
    debug: str => `${COLORS.BLUE}${str}${COLORS.DEFAULT}`
}

const format = (level, args) => {
    let modulename = new Error().stack
        .split('\n')[3]
        .replace(/(.+)\((.+)\)/g, '$2')
        .split('/')
        .slice(-2)
        .join('/')

    const prefix = [
        new Date().toJSON(),
        colorize[level](level),
        `[${commit}, pid=${pid}, ${modulename}]`
    ].join(' ')

    if (typeof args[0] === 'string') {
        args[0] = prefix + ' ' + args[0]
    } else {
        args.unshift(prefix)
    }

    return util.format(...args)
}

class Logger {
    constructor() {
        for (const key in LEVELS) {
            const level = LEVELS[key]
            this[level] = this.handler(level)
        }
    }
    handler(level) {
        return (...args) => {
            const formatted = format(level, args)
            switch (level) {
                case LEVELS.INFO:
                    console.log(formatted)
                    break

                case LEVELS.DEBUG:
                    if (argv.debug) {
                        console.log(formatted)
                    }
                    break

                case LEVELS.ERROR:
                case LEVELS.WARN:
                    console.error(formatted)
                    break
            }
        }
    }
}

module.exports = new Logger()
module.exports.delta = ts => (Date.now() - ts) / 1000
