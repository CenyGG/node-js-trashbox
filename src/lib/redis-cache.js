const bluebird = require('bluebird')
const Redis = require('ioredis')
const config = require('../config')
const log = require('./log')

const WARMING_KEY_TTL = 3

module.exports = {
  client: new Redis(config.get('redis')),

  async getCachedValue(key, lifetime, getDataFunc, cached = true) {
    let data = cached ? await this.get(key) : null

    if (data === null) {
      // check if data already requested
      const keyWarming = `${key}:warming`
      const count = await this.inc(keyWarming, WARMING_KEY_TTL)
      if (count > 1) {
        await bluebird.delay(300)
        return await this.getCachedValue(key, lifetime, getDataFunc)
      }

      // request data
      try {
        data = await getDataFunc()
      } catch (e) {
        throw e
      } finally {
        await this.del(keyWarming)
      }

      // write data to cache and reset lock
      await this.set(key, data, lifetime)
      await this.del(keyWarming)
    }

    return data
  },

  async del(key) {
    return this.client.del(key).catch((e) => {
      log.error('cache del key', key, e)
    })
  },

  async get(key) {
    const data = await this.client.get(key)
    return data ? JSON.parse(data) : null
  },

  async set(key, data, lifetime = null) {
    const args = [key, JSON.stringify(data)]
    if (lifetime !== null) {
      args.push('EX', lifetime)
    }
    await this.client.set(...args)
  },

  async inc(key, lifetime = null) {
    let pipe = this.client.pipeline().incr(key)
    if (lifetime !== null) {
      pipe = pipe.expire(key, lifetime)
    }
    const [[err, result]] = await pipe.exec()
    return result
  },

  async splice(key, limit) {
    const result = await this.client.pipeline().lrange(key, 0, limit - 1).ltrim(key, limit, -1).exec()
    const [err, rows] = result[0]
    if (err) {
      throw err
    }
    return rows
  }
}
