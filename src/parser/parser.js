import ParserService from './ParserService';

const urls = [
    "https://www.amazon.com/HP-Convertible-Touchscreen-Quad-Core-Bluetooth/dp/B0777GLYJ5/",
    "https://www.amazon.com/Fashioncraft-1950-Elephant-Bottle-Stopper/dp/B00DZ1IWYC/"
];

let parseService = new ParserService(urls, 5);
(async () => {
    await parseService.start();
})();