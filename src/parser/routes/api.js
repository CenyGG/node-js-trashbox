var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser')

import {parserService} from '../services'

router.get('/', function (req, res) {
    res.send('This is default api page.');
});

router.post('/addUrl', bodyParser.urlencoded(), function (req, res) {
    if (!req.body.url) return res.sendStatus(400)
    try {
        parserService.add(req.body.url)
    } catch (err) {
        console.log(err)
        return res.sendStatus(500)
    }
    res.sendStatus(200);
});

router.post('/deleteUrl', bodyParser.urlencoded(), function (req, res) {
    if (!req.body.url) return res.sendStatus(400)
    try {
        parserService.delete(req.body.url)
    } catch (err) {
        console.log(err)
        return res.sendStatus(500)
    }
    res.sendStatus(200);
});

router.get('/start', function (req, res) {
    try {
        parserService.start();
    } catch (e) {
        console.log(e);
        res.sendStatus(500)
    }
    res.send('Start parsing');
});

router.get('/stop', function (req, res) {
    try {
        parserService.stop();
    } catch (e) {
        console.log(e);
        res.sendStatus(500)
    }
    res.send('Stop parsing');
});


export {router}