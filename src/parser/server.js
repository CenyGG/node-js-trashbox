var express = require('express');
var app = express();

import {parserService} from './services'
import {api} from './routes'

const port = process.env.PORT || 3000
parserService.start();

app.use('/api', api)

app.get('/', function (req, res) {
  res.send('Hello World!');
});

app.listen(port, function () {
  console.log(`Example app listening on port ${port}!`);
});