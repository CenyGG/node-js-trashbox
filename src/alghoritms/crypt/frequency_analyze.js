function analizy(str) {
    const res = {}
    for (let q of str) {
        if (res[q] == undefined) res[q] = 1
        else res[q] += 1
    }
    for (let sym of this.ignore_symbols)
        delete res[sym]

    var sortable = [];
    for (var q in res) {
        sortable.push([q, res[q], res[q] / str.length]);
    }
    return sortable.sort((a, b) => {
        return b[1] - a[1];
    });
}

function find_letter_indexes(letter, str) {
    var indices = [];
    for (var i = 0; i < str.length; i++) {
        if (str[i] === letter) indices.push(i);
    }

    return indices
}

function _toString(arr) {
    let str = ''
    for (let q of arr) { str += q }
    return str
}

function decode(str) {
    this.str = str
    this.freq = this.analizy(str)
    this.letter_to_indexes = {}
    this.changes = {}
    let counter = 0

    for (let letter of this.ignore_symbols) {
        this.letter_to_indexes[letter] = this.find_letter_indexes(letter, str)
    }

    for (let letter of this.freq) {
        this.letter_to_indexes[this.alphabet[counter]] = this.find_letter_indexes(letter[0], str)
        this.changes[letter[0]] = this.alphabet[counter]
        counter++
    }
}

function i_know_letter(letter_before, letter_after) {
    this.changes[letter_before] = letter_after
    this.letter_to_indexes[letter_after] = this.find_letter_indexes(letter_before, this.str)
}

function render_str() {
    let arr = new Array(this.str.length)
    for (let q in this.letter_to_indexes) {
        let indexes = this.letter_to_indexes[q]
        for (let indx of indexes) {
            arr[indx] = q
        }
    }

    return this._toString(arr)
}

function start(str) {
    this.decode(str)
    return this.render_str()
}

function FreqAnalyzer(alphabet, ignore_symbols = ' 123456789,.:!@#$%^&*()"\';{}[]/') {
    this.alphabet = alphabet
    this.ignore_symbols = ignore_symbols
}
Object.assign(FreqAnalyzer.prototype, { start, render_str, _toString, decode, find_letter_indexes, analizy })

module.exports = FreqAnalyzer
