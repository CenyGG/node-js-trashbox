function rot(str, rot_number) {
    if (rot_number > this.alphabet.length)
        rot_number = rot_number - this.alphabet.length

    let res_str = ''
    for (let letter of str) {
        res_str += this.getLetter(letter, rot_number)
    }
    return res_str
}

function getLetter(letter, rot_number) {
    if (this._cache[letter]) return this._cache[letter]

    const cur_index = this.alphabet.indexOf(letter)
    if (cur_index < 0)
        return this._cache[letter] = letter
    else if (this.alphabet.length - 1 < cur_index + rot_number)
        return this._cache[letter] = this.alphabet[cur_index + rot_number - this.alphabet.length]
    else
        return this._cache[letter] = this.alphabet[cur_index + rot_number]
}

function clearCache() {
    this._cache = {}
}

function RotClass(alphabet) {
    this.alphabet = alphabet
    this.clearCache()
}
Object.assign(RotClass.prototype, { rot, getLetter, clearCache })

module.exports = RotClass

