alph1 = ' йцукенгшщзхъфывапролджэ\\ячсмитьбю.'
alph2 = ' qwertyuiop[]asdfghjkl;\'\\zxcvbnm,./'

function simple_change(str, alphabet1 = alph1, alphabet2 = alph2) {
    let _chache = {}
    let res = ''
    for (let letter of str) {
        if (_chache[letter]) {
            res += _chache[letter]
            continue
        }
        const index = alphabet1.indexOf(letter)
        if (index < 0) {
            _chache[letter] = letter
            continue
        }
        _chache[letter] = alphabet2[index]
        res += _chache[letter]
    }

    return res
}


module.exports = simple_change