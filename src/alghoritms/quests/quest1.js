function getSequence(str1, str2) {
    const arr1 = str1.split(',');
    const arr2 = str2.split(',');

    let i =0;
    let arr3 = arr2.map(n => i = arr2[i]);

    let result = '%s-%s-%s-%s-%s-%s';

    arr3.forEach(n => {
        result = result.replace('%s', arr1[n]);
    })

    return result;
}

let str1 = '3,0,5,4,2,1'
let str2 = '42,55,53,34,50,167'

console.log(getSequence(str2, str1))