

function reversePrint(linkedList) {
    const values = []
    values.push(linkedList.value)
    while (!!linkedList.next) {
        linkedList = linkedList.next
        values.push(linkedList.value)
    }
    const leng = values.length

    for (let i = 0; i < leng; i++) {
        console.log(values.pop())
    }
}

function reversePrint1(linkedList){
    if(!!linkedList.next) reversePrint(linkedList.next)
    console.log(linkedList.value)
}

var someList = {
    value: 1,
    next: {
        value: 2,
        next: {
            value: 3,
            next: {
                value: 4,
                next: null
            }
        }
    }
};

reversePrint(someList)
reversePrint1(someList)

