const {exec} = require('child_process')

const arr = ['echo 1', 'echo 2', 'echo 3']
arr.reduce((res, cur)=>{
    return res.then(createPromise(cur))
}, new Promise((h)=>h()))

function createPromise(cmd){
    return new Promise((h)=>{
        exec(cmd, (err, stdout, stder)=>{
            console.log(stdout)
            h();
        })
    })
}
