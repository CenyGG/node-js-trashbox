function merge_sort(arr) {
    return recursive(arr)
}


function recursive(arr) {
    let first_part
    let second_part
    if (arr.length > 2) {
        first_part = recursive(arr.slice(0, arr.length / 2))
        second_part = recursive(arr.slice(arr.length / 2))
    } else {
        if (arr[0] > arr[1]) return arr.reverse()
        return arr
    }
    let res = []
    let offset = 0
    for (let i = 0; i < first_part.length; i++) {
        for (let j = offset; j < second_part.length; j++) {
            if (first_part[i] > second_part[j]) {
                res.push(second_part[j])
                offset = j + 1
            }
            else break
        }
        res.push(first_part[i])
    }
    for (let j = offset; j < second_part.length; j++) {
        res.push(second_part[j])
    }
    return res
}

module.exports = merge_sort