function js_default_sort(arr) {
    arr = arr.slice()
    return arr.sort((a,b)=>a-b)
}

module.exports = js_default_sort