const merge_sort = require('./merge_sort')
const buble_sort = require('./buble_sort')
const js_default_sort = require('./js_sort')
const rand_arr = require('./random_array')
const o_n_sort = require('./o_n_sort')

function test(fun, arr){
    const res = fun(arr);
    for (let i=0;i<res.length-1;i++){
        if(res[i]>res[i+1]) throw Error(`Wrong sorting order! ${fun.name}. arr[${i}] = ${res[i]} , arr[${i+1}] = ${res[i+1]}`)
    }
    console.log(`${fun.name} OK.`)
    console.log(res)
}

const arr = rand_arr(1000)
test(merge_sort, arr)
test(buble_sort, arr)
test(js_default_sort, arr)
test(o_n_sort, arr)
