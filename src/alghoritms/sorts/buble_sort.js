function buble_sort(arr) {
    arr = arr.slice()
    for (let i = 0; i < arr.length; i++) {
        for (let j = i+1; j < arr.length; j++) {
            if (arr[j] < arr[i]) swap(arr, i, j)
        }
    }
    return arr
}

function swap(arr, i, j) {
    const temp = arr[i]
    arr[i] = arr[j]
    arr[j] = temp
}

module.exports = buble_sort