const {_}  = require('lodash');



function o_n_sort(arr) {
    let min, max;
    min = max = arr[0]
    for (let i = 0; i < arr.length; i++) {
        if (arr[i] > max) max = arr[i]
        if (arr[i] < min) min = arr[i]
    }

    let arr2 = new Array(max - min);
    for (let i = 0; i < arr.length; i++) {
        if (!arr2[arr[i] - min]) arr2[arr[i] - min] = [arr[i]]
        else arr2[arr[i] - min].push(arr[i])
    }
    const res = [];
    for(let i = 0; i < arr2.length; i++){
        if(arr2[i] && arr2[i].length>0){
            for(let j=0;j<arr2[i].length;j++){
                res.push(arr2[i][j])
            }
        }
    }

    return res;
}

module.exports = o_n_sort
