const merge_sort = require('./merge_sort')
const buble_sort = require('./buble_sort')
const js_default_sort = require('./js_sort')
const rand_arr = require('./random_array')
const o_n_sort = require('./o_n_sort')

function estimate_time(sort, name, arr) {
    let start = new Date()
    sort(arr)
    let fin = new Date()
    console.log(`Estimated time for ${name} is : ${fin - start}`)
}

const arr = rand_arr(100000)
estimate_time(js_default_sort, "js_default_sort", arr)
estimate_time(merge_sort, "merge_sort", arr)
estimate_time(buble_sort, "buble_sort", arr)
estimate_time(o_n_sort, "o_n_sort", arr)
